var express = require('express');
const authorize = require("../modules/auth_checker");
var router = express.Router();


/* GET users login. */

router.post('/', async function (req, res, next) {
    const authData = req.body;
    const login = authData.login;
    const password = authData.password;

    // const authRes = await authorize(login, password);
    // console.log(authRes, 'authres');
    /*
    {
     coockie - sessionID
     body: {
        loginError: true/false
        passwordError: true/false
     }
    }
     */
    //
    // if (authRes.coockie === '') {
    //     res.status(400).json(authRes.body);
    // } else {
    //     res.cookie('sessionID', authRes.coockie)
    // }


    const sessionID = await authorize(login, password);
    if (sessionID === '') {
        res.status(400);
    } else {
        res.cookie("sessionID", sessionID)
    }

    // res.json(JSON.stringify(authRes.body));

    res.send();
});

    router.get("/logout", function (req, res, next) {
        console.log("обработка")
        res.clearCookie('sessionID');
        res.send('Cookie удалена');
    });



module.exports = router;
