const authMiddleware = function (req, res, next) {
    if (!req.cookies.sessionID) {
        res.status(401);
        res.send();
        return
    }
    next();
}

module.exports = authMiddleware;

