const bcrypt = require('bcrypt');

const authorize = async (login, password) => {

    // console.log(login);
    // console.log(password);
    // const res = {
    //     body: {}
    // };
    //
    // if (login !== admin.login) {
    //     res.body.loginError = true;
    // } else {
    //     res.body.loginError = false;
    // }
    //
    // if (password !== admin.passwordHash) {
    //     res.body.passwordError = true;
    // } else {
    //     res.body.passwordError = false;
    // }
    //
    // if (!res.body.loginError && !res.body.passwordError) {
    //     res.coockie = new Date().toISOString();
    // } else {
    //     res.coockie = '';
    // }
    //
    // return res;

    if (login === admin.login && await bcrypt.compare(password, admin.passwordHash)) {
        return new Date().toISOString()
    }
    return ''
}

const admin = {
    "login": "Nikita",
    "passwordHash": "$2b$10$yLvfeKOhKHvlURERAAAI.OIz5.NAgNSKodjW4Gp6dm7MLylfmwOtK"
}

module.exports = authorize;
