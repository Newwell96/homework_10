import React, {useEffect, useState} from 'react';
import './index.css';
import Authorization from "./components/pages/auth/auth";
import Header from "./components/pages/header/header";
import GameStore from "./components/store/GameStore";
import {GameContext} from "./components/store/GameStore";

function App() {

    const [currentUser, setCurrentUser] = useState(null);
    const [isAuthorized, setIsAuthorized] = useState(null);


    useEffect(() => {
        if (window.document.cookie.indexOf('sessionID') !== -1) {
            setIsAuthorized(true);
        } else {
            async function fetchData() {
                try {
                    const response = await fetch('/users/me');
                    if (!response.ok) {
                        if (response.status === 401) {
                            // Обработка ошибки 401
                            setIsAuthorized(false)
                        } else {
                            console.log(`Ошибка ${response.status}: ${response.statusText}`);
                        }
                    } else {
                        // Обработка успешного ответа
                        const data = await response.json();
                        setIsAuthorized(true)
                        setCurrentUser(data)
                    }
                } catch (error) {
                    // Обработка ошибок сети
                    console.error('Ошибка сети:', error);
                }
            }
            fetchData();
        }
    }, []);

    let gameStore = null;
    if (isAuthorized) {
        gameStore = new GameStore();
    }

    // console.log(currentUser)

    if (!isAuthorized) {
        return (
            <Authorization
                setIsAuthorized={setIsAuthorized}
            />
        );
    }

    return (
    <GameContext.Provider value={gameStore}>
        <Header
            isAuthorized = {isAuthorized}
            setIsAuthorized = {setIsAuthorized}
        />
        </GameContext.Provider>

    );
}

export default App;