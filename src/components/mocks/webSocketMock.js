// Класс Broker предоставляет механизм для регистрации и вызова обратных вызовов.
// emit(type, payload) - метод, который вызывает все зарегистрированные обратные вызовы для заданного типа с указанным payload.
// on(type, cb) - метод, который регистрирует обратный вызов cb для заданного типа.
import {mockApiCall} from "./gameMock";

class Broker {
    constructor() {
        this.listeners = {};
    }

    emit(type, payload) {
        if (this.listeners[type]) {
            this.listeners[type].forEach((cb) => cb(type, payload, this));
        }
    }

    on(type, cb) {
        if (!this.listeners[type]) {
            this.listeners[type] = [];
        }
        this.listeners[type].push(cb);
    }
}

class mockGameWS {
    constructor() {
        this.broker = new Broker();
        this.broker.on('send-message', this.onMessageSent.bind(this));
        this.broker.on('make-move', this._handleMakeMove.bind(this));
        this.broker.on('move-made', this.onMoveMade.bind(this));
        this.broker.on('move-failed', this.onMoveFailed.bind(this));
        this.broker.on('error', this.onError.bind(this));
    }

    async sendMessage(userId, text) {
        console.log(`--Сервер получил сообщение от ${userId}--`);
            try {
                await mockApiCall();
                this.broker.emit('send-message', {userId, text});
            }
        catch (error) {
            this._handleError("Произошла ошибка отправки сообщения.")
        }
    }

    onMessageSent(type, payload) {
        console.log(`${payload.userId} отправил сообщение: ${payload.text}`,"--Ответ сервера--");
        // this.broker.emit('message-sent', payload);
    }

    async makeMove(userId, row, col, cell) {
        console.log(`Игрок ${userId} сделал ход ${row}:${col}`,"--Сообщение серверу--");
        try {
            await mockApiCall();
            this.broker.emit('make-move', {userId, row, col, cell});
        }
        catch (error) {
            this._handleError(error, "Ошибка хода.")
        }
    }

    onMoveMade(type, payload) {
        console.log(`Игрок ${payload.userId} сделал ход ${payload.row}:${payload.col}`,"--Ответ сервера--");
    }

    onMoveFailed(type, payload) {
        console.log("Ход невозможен","--Ответ сервера--" );
    }

    onError(type, payload) {
        console.error(payload.error, payload.reason,"--Ответ сервера--" );
    }

    _handleMakeMove(type, payload) {
        if (!payload.cell) {
            this.broker.emit('move-made', {userId: payload.userId, row: payload.row, col: payload.col});
        } else {
            this.broker.emit('move-failed', {});
        }
    }

    _handleError(error, reason) {
        this.broker.emit('error', {error, reason});
    }
}

const gameWebSocket = new mockGameWS();
export default gameWebSocket;

