import playersData from "../pages/gamePanel/data/playersData.json"
const playersList = playersData.users;

export const mockApiCall = (data) => new Promise((resolve, reject)=> {
    setTimeout(() => {
        if (Math.random() < 0.1){
            reject(new Error());
        } else {
            resolve(data);
        }
    }, Math.random()*3000)
});

const mockGameApi = {
    //запрос на получение информации о игроках в рамках игровой сессии
    getPlayersInfo: async () => {
        let check = true;
        while (check) {
            try {
                const response = await mockApiCall( playersList);
                check = !check;
                return response;
            } catch (error) {
                console.error(error,"Произошла ошибка загрузки инфо. Повторяем запрос.");
                // alert("Произошла ошибка загрузки инфо. Повторяем запрос.");
            }
        }
        },

    sendFieldState:  (field) => {
            localStorage.setItem("currentField", JSON.stringify(field));
    },

    getFieldState: async () => {
        let check = true;
        while (check) {
        try {
            const currentField = localStorage.getItem("currentField");
            const response = await mockApiCall( currentField);
            check = !check;
            return response;
        } catch (error) {
            console.error(error,"Произошла ошибка загрузки поля. Повторяем запрос.");
            // alert("Произошла ошибка загрузки поля. Повторяем запрос.");
        }
    }},

    sendChatMessage: (message) => {
        localStorage.setItem("chat", JSON.stringify(message));
    },
    getChatMessage: async () => {
        let check = true;
        while (check) {
        try {
            const messages = localStorage.getItem("chat");
            const response = await mockApiCall(messages);
            check = !check;
            return response;
        } catch (error) {
            console.error(error, "Произошла ошибка загрузки чата. Повторяем запрос.");
            // alert("Произошла ошибка загрузки чата. Повторяем запрос.");
        }
    }},

    sendXisNext: (status) => {
        localStorage.setItem("xIsNext", JSON.stringify(status));
    },
    getXisNext: async () => {
        let check = true;
        while (check) {
        try {
            const xIsNext = localStorage.getItem("xIsNext");
            const response = await mockApiCall(xIsNext);
            check = !check;
            return response;
        } catch (error) {
            console.error(error,"Произошла ошибка загрузки статуса. Повторяем запрос.");
            // alert("Произошла ошибка загрузки статуса. Повторяем запрос.");
        }
    }},
};

export default mockGameApi;

