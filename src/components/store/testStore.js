import {autorun, observable, action, makeObservable, computed, reaction, when} from "mobx";

class Game {
    mode = "X";
    field = [
        [null, null, null],
        [null, null, null],
        [null, null, null]
    ];
    constructor () {
        makeObservable(
            this,
            {
                mode: observable,
                field: observable,
                makeMove: action,
                winner: computed,
            }
        )
    }

    get winner() {
        if (this.isWinner("X")) {
            return "X";
        }
        if (this.isWinner("O")) {
            return "O";
        }
        return null
    }

    makeMove (row, col){
        this.field[row][col] = this.mode
        this.mode = this.mode === "X" ? "O" : "X";
    }

    isWinner(symbol) {
        const combs = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        return combs.some (
            (comb) => comb.every((elem) => {
                const row = Math.floor(elem/3);
                const col = elem % 3;
                return this.field[row][col] === symbol;
            })
        );
    }
}

const game = new Game();

autorun(
    () =>{
        game.winner;
        console.log("autorun")
    });

reaction(
    () => game.winner,
    () => {
        console.log("reaction")
    }
);

when (
    () => game.winner,
    () => {
        console.log("when");
    }
);

game.makeMove(0,0)
game.makeMove(1,1)
game.makeMove(0,1)
game.makeMove(2,1)
game.makeMove(0,2)
