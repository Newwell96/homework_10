import React from 'react';
import './chip.css'

function Chip({
                  className,
                  text,
                  backgroundColor,
                  fontColor,

}) {
    return (
        <div
            className={className}
            style={{
                background: backgroundColor,
                color: fontColor
            }}
        >
            {text}
        </div>
    );
}

export default Chip;
