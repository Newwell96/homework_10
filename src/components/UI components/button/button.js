import React from "react";
import './button.css';
import MyImage from "../image/image";

function MyButton ({
   onClickFunction,
   buttonText,
   className,
   buttonID,
   backgroundColor,
    fontColor,
    imageName,
    imageWidth,
    imageHeight,
}) {
    return (
        <button
            className={className}
            onClick={onClickFunction}
            id={buttonID}
            style={
            {
                backgroundColor: backgroundColor,
                color: fontColor,
            }
        }
        >
            {imageName && <MyImage
                name={imageName}
                width={imageWidth}
                height={imageHeight}
            />}
            {buttonText}

        </button>
    );
}

export default MyButton;