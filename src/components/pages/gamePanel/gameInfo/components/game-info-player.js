import React from "react";
import './game-info-player.css';
import MyImage from "../../../../UI components/image/image";

function MyPlayerInfo (
    {
        pic,
        playerName,
        percentWin,
    }
) {
    function getPic () {
        if (pic === "X") {
            return "miniX"
        } else if (pic === "O") {
            return "miniZero"
        }
    }
    return (
                <div className="subject-container">
                    <div>
                        <MyImage
                            name={getPic()}
                            width={24}
                            height={24}
                        />
                    </div>
                    <div className="subject-info">
                        <div className="subject-name">{playerName}</div>
                        <div className="subject-percent">{percentWin}</div>
                    </div>
                </div>
    )
}

export default MyPlayerInfo;
