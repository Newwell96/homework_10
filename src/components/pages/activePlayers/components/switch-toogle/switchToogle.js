import React from 'react';
import "./switchToogle.css"

function MySwitchToggle (
    {
        filterStatus,
        setFilterStatus
    }
) {

    return (
        <label className="switch">
            <input
                onClick={
                    () => setFilterStatus(!filterStatus)
            }
                type="checkbox"/>
                <span className="slider round"></span>
        </label>
    );
}

export default MySwitchToggle;