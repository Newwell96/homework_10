import React, {useEffect, useState} from 'react';
import '../../../index.css';
import MySwitchToggle from "./components/switch-toogle/switchToogle";
import ActivePlayersRow from "./components/activePlayerRow/activePlayersRow";
import activeData from "./data/active.json"

function ActivePlayers() {

    const [activeList, setActiveList] = useState(activeData["activePlayers"]);
    const [isOnlyFreeFilter, setIsOnlyFreeFilter] = useState(false);

    useEffect(() => {
    }, [isOnlyFreeFilter]);

    function changePlayerStatus(index) {
        const updatedActiveList = [...activeList];
        updatedActiveList[index].status = !updatedActiveList[index].status;
        setActiveList(updatedActiveList);
    }

    function renderRows(data) {
        return data.map((item, index)=> {
            if (isOnlyFreeFilter) {
                if (item["status"]) {
                    return (
                        <ActivePlayersRow
                            name={item["name"]}
                            status={item["status"]}
                            onChangeStatus = {
                                () => changePlayerStatus(index)
                            }
                        />
                    );
                }
            } else {
                return (
                    <ActivePlayersRow
                        name={item["name"]}
                        status={item["status"]}
                        onChangeStatus = {
                            () => changePlayerStatus(index)
                        }
                    />
                );
            }
        });
    }

    return (
        <div className="tables_body">
            <div id="tables_container" class="fixed">
                <div className="headTable">
                    <h1 className="scores_title">Активные игроки</h1>
                    <div className="onlyFreeText">
                        Только свободные
                        <MySwitchToggle
                            filterStatus = {isOnlyFreeFilter}
                            setFilterStatus = {setIsOnlyFreeFilter}
                        />
                    </div>
                </div>
                <table className="table">
                    <tbody>

                    {renderRows(activeList)}

                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default ActivePlayers;