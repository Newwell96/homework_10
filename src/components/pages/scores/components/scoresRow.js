import React from 'react';
import './scoresRow.css';

const ScoresRow = (props) => {
    return (
        <tr className="score_row">
            <td className="first_colomn">
                {props.name}
            </td>
            <td className="tipic_colomn">
                {props.total}
            </td>
            <td className="tipic_colomn" id="green-color">
                {props.wins}
            </td>
            <td className="tipic_colomn" id="red_color">
                {props.loses}
            </td>
            <td className="last_colomn">
                {props.percent}%
            </td>
        </tr>
    );
};

export default ScoresRow;
