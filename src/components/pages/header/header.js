import React from 'react';
import './header.css';
import {
    BrowserRouter as Router, Route, Link, Routes
} from "react-router-dom";
import Scores from "../scores/scores";
import XOPanel from "../gamePanel/xo-panel";
import Auth from "../auth/auth";
import ActivePlayers from "../activePlayers/activePlayers";
import GameHistory from "../gameHistory/gameHistory";
import PlayerList from "../playersList/players_list";
import MyImage from "../../UI components/image/image";
import ExitButton from "./exitButton/exitButton";
import GameStore from "../../store/GameStore";
import {GameContext} from "../../store/GameStore";

function Header({isAuthorized, setIsAuthorized}) {

    const routerMap = {
        "/":0,
        "/scores":1,
        "/active_players":2,
        "/gameHistory": 3,
        "/players_list": 4
    }

    const currentPathName = window.location.pathname ;
    const currentPathIndex = routerMap[currentPathName] || 0;

    const [state, setState] = React.useState({activeItem: currentPathIndex});
    const handleClick = (event, {index}) => setState({activeItem: index});

    async function deleteCookie() {
        console.log(await fetch('/auth/logout', {
            method: 'GET'
        }))
        // .then(response => {
        //     console.log(response, "статус");
        //     // handle success
        // })
        // .catch(error => {
        //     console.error(error, "ошибка");
        //     // handle error
        // });
    }

    return (

        <Router>

        <header>
            <div id="logo"><MyImage
                name="xoLogo"
            /></div>
            <div id="nav-panel">
                <Link
                    className={state.activeItem === 0 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 0})}
                    to="/">
                    Игровое поле
                </Link>

                <Link
                    className={state.activeItem === 1 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 1})}
                    to="/scores">
                    Рейтинг
                </Link>

                <Link
                    className={state.activeItem === 2 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 2})}
                    to="/active_players">Активные игроки</Link>

                <Link
                    className={state.activeItem === 3 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 3})}
                    to="/gameHistory">
                    История игр
                </Link>

                <Link
                    className={state.activeItem === 4 ? 'active' : ''}
                    onClick={(event) => handleClick(event, {index: 4})}
                    to="/players_list">
                    Список игроков
                </Link>

            </div>
            <ExitButton
                onClickFunction={() => {
                    deleteCookie();
                    localStorage.clear();
                    setIsAuthorized(false);
                }}
            />

        </header>
        <Routes>
            <Route path="/" element={<XOPanel/>}/>
            <Route path="/scores" element={<Scores/>}/>
            <Route path="/auth" element={<Auth/>}/>
            <Route path="/active_players" element={<ActivePlayers/>}/>
            <Route path="/gameHistory" element={<GameHistory/>}/>
            <Route path="/players_list" element={<PlayerList/>}/>
        </Routes>

    </Router>
    )
}

export default Header;